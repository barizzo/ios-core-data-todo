//
//  DetailViewController.swift
//  TodoList
//
//  Created by Brian Rizzo on 8/24/16.
//  Copyright © 2016 Brian Rizzo. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	@IBOutlet weak var textField: UITextField!

	var item: Item?

	override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
		guard let item = item else { fatalError("Cannot show detail without an item") }
		textField.text = item.text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@IBAction func save(sender: AnyObject) {
		if let item = item {
			item.text = textField.text
			DataController.sharedInstance.saveContext()

			self.navigationController?.popViewControllerAnimated(true)
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
